package main

import (
  "os"
  "fmt"
  "time"
  "strconv"
)

const poison_pill = -1
 
func pinger(n int64, in chan int64, out chan int64, end chan bool, done chan bool) {
  exit := false

  for !exit {
    select {
    case value := <- in :
      next := value + 1
      fmt.Printf("[PINGER] ping! %d \n",next)    
      if next > n {
        end <- true
        exit = true
      } else {
        out <- next
      }
    case  <- end :
      exit = true
    }
  }
  fmt.Println("[PINGER] end.\n")   
  done <- true
}

func ponger(n int64, in chan int64, out chan int64, end chan bool, done chan bool) {
  exit := false

  for !exit {
    select {
    case value := <- in :
      next := value + 1
      fmt.Printf("[PONGER] pong! %d \n",next)    
      if next > n {
        end <- true
        exit = true
      } else {
        out <- next
      }
    case  <- end :
      exit = true
    }
  }
 fmt.Println("[PONGER] end.\n")    
  done <- true
}


func main() {

 n,_ := strconv.ParseInt(os.Args[1], 10, 64)

  var pingch chan int64 = make(chan int64)
  var pongch chan int64 = make(chan int64)
  var done chan bool = make(chan bool)
  var end chan bool = make(chan bool)
  // var n int64 = 1000

  t0 := time.Now()

  go pinger(n, pongch, pingch, end, done)
  go ponger(n, pingch, pongch, end, done)

  pongch <- 1
  
  <- done
  <- done

  t1 := time.Now()

  fmt.Printf("Done %d in %v \n", n, t1.Sub(t0))
}
