package pcd.lab12.rest_services.jaxrs;

import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;


@Path("myconsservice")
public class MyConsumerService {
	
	public MyConsumerService(){
        Logger.getGlobal().setLevel(Level.ALL);
	}

	@POST
	@Path("results/{resId}")
	@Produces(MediaType.APPLICATION_JSON)
	public String notifyResult(String msg) {
		try {
			JsonObject res = parseJSON(msg);
	        String id = res.get("resId").toString();
	        double result = res.getJsonNumber("result").doubleValue();
	        log("[POST NEW RES] res for "+id+": "+result);
	        
	        JsonObject ret = Json.createObjectBuilder()
	        		.add("status", "ok")
	        		.build();			
	        return ret.toString();
		} catch (Exception ex){
			JsonObject ret = Json.createObjectBuilder()
	        		.add("status", "error")
	        		.build();			
	        return ret.toString();
		}
	}

    private JsonObject parseJSON(String s) throws Exception {
        JsonReader reader = Json.createReader(new StringReader(s));        
        JsonObject jsonObj = reader.readObject();         
        reader.close();
        return jsonObj;
    }
    
    private void log(String msg){
    	Logger.getGlobal().log(Level.INFO,msg);
    }
}