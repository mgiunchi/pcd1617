package pcd.lab12.rest_services.jaxrs;

import static javax.ws.rs.client.Entity.json;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.logging.Logger;

import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.persistence.services.websphere.MBeanWebSphereRuntimeServices;

public class MyConsumerAgent extends Thread {

	static private MyConsumerAgent instance;
	private ArrayBlockingQueue<JsonObject> msgQueue;

	public MyConsumerAgent() {
		msgQueue = new ArrayBlockingQueue<JsonObject>(50);
	}

	public void run() {
		log("running.");
		try {

			log("setting up the service.");
			/* setup the service */
			setupService(8082);

			/* ready to go */

			log("making a req.");
			Client client = ClientBuilder.newClient();
			WebTarget myRes = client.target("http://localhost:8080/myservice/requestsWithHook");

			JsonObject msg = Json.createObjectBuilder().add("input", 16)
					.add("resURI", "http://localhost:8082/agserv/results/myres").build();

			log("sending request: " + msg);

			/* sync, can be made async */
			
			Invocation req = myRes.request(MediaType.APPLICATION_JSON).buildPost(json(msg.toString()));
			String ret = req.invoke(String.class);

			log("service req ret " + ret);

			try {
				JsonObject msgRes = waitForMsg();
				double result = msgRes.getJsonNumber("result").doubleValue();
				log("got result: " + result);

			} catch (Exception ex) {
				ex.printStackTrace();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	protected JsonObject waitForMsg() throws InterruptedException {
		return msgQueue.take();
	}

	protected void setupService(int port) {

		ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
		context.setContextPath("/");
		Server jettyServer = new Server(port);

		jettyServer.setHandler(context);

		ServletHolder jerseyServlet = context.addServlet(org.glassfish.jersey.servlet.ServletContainer.class, "/*");

		jerseyServlet.setInitOrder(0);

		jerseyServlet.setInitParameter("jersey.config.server.provider.classnames",
				MyConsumerAgentService.class.getCanonicalName());

		new Thread(() -> {
			try {
				jettyServer.start();
				jettyServer.join();
			} catch (Exception ex) {
			} finally {
				jettyServer.destroy();
			}
		}).start();
	}

	/* external interface */

	public void dispatchMsg(JsonObject msg) throws InterruptedException {
		msgQueue.put(msg);
	}

	public void log(String msg) {
		System.out.println("[CONS AGENT] " + msg);
	}

	public static MyConsumerAgent getInstance() {
		synchronized (MyConsumerAgent.class) {
			if (instance == null) {
				instance = new MyConsumerAgent();
			}
			return instance;
		}
	}

}
