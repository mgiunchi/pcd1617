package pcd.ass02.ex2.sol;


public class Controller {
	
	private Master master;
	private MandelbrotSetImage image;
	private MandelbrotView view;
	private StopFlag stopFlag;
	private int nTasks;
	
	public Controller(MandelbrotSetImage image, MandelbrotView view){
		this.image = image;
		this.view = view;
		stopFlag = new StopFlag();
		nTasks = Runtime.getRuntime().availableProcessors();
		nTasks = 128;
	}
	
	public void processEvent(String event) {
		try {
			if (event.equals("start")){
				view.setRunningModality(true);
				stopFlag = new StopFlag();
				master = new Master(image,view,stopFlag, nTasks);
				master.start();
			} else if (event.equals("stop")){
				view.setRunningModality(false);
				stopFlag.set();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
