package pcd.ass01.ex2.sol;

import java.util.concurrent.Semaphore;

public class Ponger extends Player {
	public Ponger(Counter counter, Semaphore iCanGo, Semaphore friendCanGo, StopFlag stop, Output output, Semaphore shutdown){
		super("pong!",counter,iCanGo,friendCanGo,stop,output, shutdown);
	}	
}
