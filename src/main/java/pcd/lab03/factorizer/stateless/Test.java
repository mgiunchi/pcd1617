package pcd.lab03.factorizer.stateless;

import pcd.lab03.factorizer.FactorizerService;
import pcd.lab03.factorizer.ServiceUser;

public class Test {

	public static void main(String[] args) {
		
		FactorizerService service = new StatelessFactorizer();
		for (int i = 0; i < 2; i++){
			new ServiceUser(service,1000000).start();
		}
		
	}
	
}
