package pcd.ass03.ex3;

import io.reactivex.Flowable;

public interface ObservableTemperatureSensor extends TemperatureSensor {
	
	Flowable<Double> createObservable();
	
}
