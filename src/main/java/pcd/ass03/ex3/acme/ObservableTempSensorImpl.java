package pcd.ass03.ex3.acme;

import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.flowables.ConnectableFlowable;

/**
 * Class implementing a simulated temperature sensor
 * 
 * @author aricci
 *
 */
class ObservableTempSensorImpl {	
	private volatile double currentValue;
	private double min, max, spikeFreq;
	private Random gen;
	private BaseTimeValue time;
	private double zero;
	private double range;
	private double spikeVar;
	private UpdateTask updateTask;
	private ScheduledExecutorService exec = Executors.newScheduledThreadPool(1);
	
	/**
	 * Create a sensor producing values in a (min,max) range, with possible spikes
	 * 
	 * @param min range min
	 * @param max range max
	 * @param spikeFreq - probability to read a spike (0 = no spikes, 1 = always spikes)
	 */
	protected ObservableTempSensorImpl(double min, double max, double speed, double spikeFreq){
		gen = new Random(System.nanoTime());
		time = BaseTimeValue.getInstance(speed);
		zero = (max + min)*0.5;
		range = (max - min)*0.5;
		this.spikeFreq = spikeFreq; 
		spikeVar = range*1000;
		updateTask = new UpdateTask();
		exec.scheduleAtFixedRate(updateTask, 0, 100, java.util.concurrent.TimeUnit.MILLISECONDS);

		/* initialize currentValue */
		updateTask.run();
	}
	
	/**
	 * Reading the current sensor value 
	 * 
	 * @return sensor value
	 */
	public double getCurrentValue(){
		synchronized (updateTask){
			return currentValue;
		}
	}

	public void shutdown(){
		exec.shutdownNow();
	}
	
	protected Flowable<Double> createObservable(int period){
		ObservableTempSensorImpl sensor = this;
		Flowable<Double> source = Flowable.create(emitter -> {
			new Thread(() -> {
				while (true){
					try {
						emitter.onNext(sensor.getCurrentValue());
						Thread.sleep(period);
					} catch (Exception ex){}
				}
			}).start();
		     //emitter.setCancellable(c::close);
		}, BackpressureStrategy.BUFFER);
		
		ConnectableFlowable<Double> hotObservable = source.publish();
		hotObservable.connect();
		return hotObservable;
	}
	
	class UpdateTask implements Runnable {
		public void run(){
			double newValue;

			double delta = (-0.5 + gen.nextDouble())*range*0.2;
			newValue = zero + Math.sin(time.getCurrentValue())*range*0.8 + delta;

			boolean newSpike = gen.nextDouble() <= spikeFreq;
			if (newSpike){
				newValue = currentValue + spikeVar;
			}

			synchronized (this){
				currentValue = newValue;
			}
		}
	}

	
	
	static class BaseTimeValue {
		static BaseTimeValue instance;
		static BaseTimeValue getInstance(double speed) {
			synchronized (BaseTimeValue.class){
				if (instance == null){
					instance = new BaseTimeValue(speed);
				}
				return instance;
			}
		}
		
		private double time;
		private ScheduledExecutorService exec = Executors.newScheduledThreadPool(1);

		private BaseTimeValue(double speed){
			time = 0;
			exec.scheduleAtFixedRate(() -> {
				synchronized (exec){
					time += 0.01*speed;
				}
			}, 0, 100, java.util.concurrent.TimeUnit.MILLISECONDS);
		}
		
		public double getCurrentValue(){
			synchronized (exec){
				return time;
			}
		}
	}	
}



