package pcd.ass03.ex3.acme;

import io.reactivex.Flowable;
import pcd.ass03.ex3.ObservableTemperatureSensor;

public class TemperatureSensorB1 extends ObservableTempSensorImpl implements ObservableTemperatureSensor {

	public TemperatureSensorB1() {
		super(0, 25, 1.8, 0.1);
	}

	public Flowable<Double> createObservable(){
		return createObservable(100);
	}
}
