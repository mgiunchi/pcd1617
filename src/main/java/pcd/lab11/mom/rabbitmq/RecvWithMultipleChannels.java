package pcd.lab11.mom.rabbitmq;
import com.rabbitmq.client.*;

import java.io.IOException;

public class RecvWithMultipleChannels {

  private final static String QUEUE_NAME = "hello";

  public static void main(String[] argv) throws Exception {
    ConnectionFactory factory = new ConnectionFactory();
    factory.setHost("localhost");
    Connection connection = factory.newConnection();
    Channel channel = connection.createChannel();
    channel.queueDeclare(QUEUE_NAME, false, false, false, null);
    System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

    Channel channel2 = connection.createChannel();
    channel2.queueDeclare(QUEUE_NAME, false, false, false, null);

    Consumer consumer = new DefaultConsumer(channel) {
      @Override
      public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
          throws IOException {
        String message = new String(body, "UTF-8");
        System.out.println(" [x] Received A '" + message + "'");
        // while (true){}
      }
    };

    channel.basicConsume(QUEUE_NAME, true, consumer);
    System.out.println("CONSUME #1 DONE.");
    

    Consumer consumer2 = new DefaultConsumer(channel) {
        @Override
        public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
            throws IOException {
          String message = new String(body, "UTF-8");
          System.out.println(" [x] Received B '" + message + "'");
        }
      };

      channel2.basicConsume(QUEUE_NAME, true, consumer2);
      System.out.println("CONSUME #2 DONE.");

  }
}
