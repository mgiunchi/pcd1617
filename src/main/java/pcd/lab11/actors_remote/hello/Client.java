package pcd.lab11.actors_remote.hello;

import java.io.File;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import akka.actor.ActorSystem;
import akka.actor.Props;

public class Client {
  public static void main(String[] args) throws Exception {
	  Config config = ConfigFactory.parseFile(new File("src/main/java/pcd/lab11/actors_remote/hello/app2.conf"));
	  ActorSystem system = ActorSystem.create("MySystem",config);
	    
	  ActorSelection selection = system.actorSelection("akka.tcp://MySystem@127.0.0.1:2552/user/myActor");
	  selection.tell(new HelloMsg("World2"),ActorRef.noSender());
  }
}
